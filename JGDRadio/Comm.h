//
//  Comm.h
//  JGD Radio
//
//  Created by Franco Santa Cruz on 3/17/14.
//  Copyright (c) 2014 Martín Mineo. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    CommErrorOk,
    CommErrorHttpError,
    CommErrorProtocolError,
} CommError;

@interface Comm : NSObject


+ (CommError)addDevice:(NSString*)pushNotDeviceToken;
+ (CommError)sendPushNot:(id)text;
+ (CommError)pingDevice;

@end
