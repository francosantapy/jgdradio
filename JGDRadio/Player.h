//
//  Player.h
//  JGDRadio
//
//  Created by Franco Santa Cruz on 3/9/14.
//  Copyright (c) 2014 Martín Mineo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>


@interface Player : NSObject

@property (retain, nonatomic) AVPlayer *player;
@property (assign, nonatomic) BOOL playing;

@property (copy, nonatomic) NSString *url;

@property (retain, nonatomic) AVPlayerItem *observingMetadataItem;


@property (copy, nonatomic) void (^readyToPlay)(void);
@property (copy, nonatomic) void (^startedPlaying)(void);

@property (copy, nonatomic) void (^errorWhileTryingToPlay)(NSError*);

@property (copy, nonatomic) void (^metadataReady)(NSArray*);

- (id)initWithURL:(NSString*)url;
- (void)play;
- (void)pause;
- (void)start;

- (BOOL)isReadyToPlay;

@end
