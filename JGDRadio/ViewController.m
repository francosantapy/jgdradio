//
//  ViewController.m
//  JGDRadio
//
//  Created by Martín Mineo on 31/01/14.
//  Copyright (c) 2014 Martín Mineo. All rights reserved.
//

#import "ViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "Player.h"
#import "WritePushNotsViewController.h"
//#import <DevaFwk/DevaFwk.h>

static id The24242348234 = @"4286265";

static id _URL_ = @"http://moon.wavestreamer.com:6821/listen.m3u?sid=1";


@interface ViewController ()
@property (assign, nonatomic) BOOL firstSwipeToRevealSecret;
@property (retain, nonatomic) IBOutlet UITextField *passwordTextField;
@property (assign, nonatomic) IBOutlet UIView *hiddenView;
- (IBAction)loginHidden:(id)sender;

@end

@implementation ViewController

- (id)init
{
    self = [super init];
    if (self) {
        

    }
    return self;
}


- (void)setTitlesWithMetadataArray:(NSArray*)metadataArray
{
    for (AVMetadataItem* metadata in metadataArray)
    {
        if( [metadata.key isEqual:@"title"] )
        {
            // Title Coming
            
            
            NSArray *keys = [NSArray arrayWithObjects:
                             MPMediaItemPropertyTitle,
                             MPMediaItemPropertyArtist,
                             MPMediaItemPropertyPlaybackDuration,
                             MPNowPlayingInfoPropertyPlaybackRate,
                             MPMediaItemPropertyAlbumTrackCount,
                             nil];
            NSArray *values = [NSArray arrayWithObjects:
                               metadata.stringValue,
                               @"JGD Radio",
                               @0,
                               @1,
                               @1,
                               nil];
            NSDictionary *mediaInfo = [NSDictionary dictionaryWithObjects:values forKeys:keys];
            [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:mediaInfo];
            
            self.musicNameLabel.text = metadata.stringValue;
            
        }
        NSLog(@"\nkey: %@\nkeySpace: %@\ncommonKey: %@\nvalue: %@", [metadata.key description], metadata.keySpace, metadata.commonKey, metadata.stringValue);
    }
}

- (void)setupPlayer
{
    
    self.player2 = [[Player alloc] initWithURL:_URL_];

    self.player2.readyToPlay = ^(){
        
        [self.player2 play];

    };
    self.player2.metadataReady = ^(NSArray* metadataArray){
        [self setTitlesWithMetadataArray:metadataArray];
    };
    self.player2.errorWhileTryingToPlay = ^(NSError *error){
        [self setUiToNotLoading];
        [[[[UIAlertView alloc] initWithTitle:@"Error" message:@"Ocurrió un error al cargar la radio, por favor volvé a intentar en unos segundos" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles: nil] autorelease] show];
    };
    
    self.player2.startedPlaying = ^(){
        [self startedPlaying];
    };
    
}

- (void)startedPlaying
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self setUiToNotLoading];
}


- (void)setPlayButtonTitle
{
    id image = self.player2.playing ? @"pause-button.png" : @"play-button.png";
    
    //[self.playButton setTitle:self.playButtonTitle forState:UIControlStateNormal];
    [self.playButton setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    
}

- (void)setupColor
{
    
    // from(rgb(65, 30, 80)), to(rgb(78, 38, 95)));
    
    
    UIView *view = self.view;//[[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)] autorelease];
    UIColor *from = [UIColor colorWithRed:65/255.0 green:30/255.0 blue:80/255.0 alpha:1];
    UIColor *to = [UIColor colorWithRed:78/255.0 green:38/255.0 blue:95/255.0 alpha:1];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[from CGColor], (id)[to CGColor], nil];
    [view.layer insertSublayer:gradient atIndex:0];
    
}

- (void)removeHiddenView
{
    int hiddenViewTag = 8889998;
    UIView* hiddenView = [self.view viewWithTag:hiddenViewTag];
    [hiddenView removeFromSuperview];
}

- (void)swipe:(UISwipeGestureRecognizer*)gr
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    int hiddenViewTag = 8889998;
    UIView* hiddenView = [self.view viewWithTag:hiddenViewTag];
    
    id directions = @{
          @(UISwipeGestureRecognizerDirectionDown):@"UISwipeGestureRecognizerDirectionDown",
          @(UISwipeGestureRecognizerDirectionUp):@"UISwipeGestureRecognizerDirectionUp",
          @(UISwipeGestureRecognizerDirectionLeft):@"UISwipeGestureRecognizerDirectionLeft",
          @(UISwipeGestureRecognizerDirectionRight):@"UISwipeGestureRecognizerDirectionRight",
    };
    
    NSLog(@"swipe, direction = %@, touches = %d", directions[@(gr.direction)], gr.numberOfTouches);
    
    if( hiddenView && gr.direction == UISwipeGestureRecognizerDirectionLeft )
    {
        // ok, noop
    }
    else if( !hiddenView )
    {
        if( !self.firstSwipeToRevealSecret
           && gr.direction == UISwipeGestureRecognizerDirectionRight
           && gr.numberOfTouches == 3 )
        {
            // ok, with the next swipe it'll be revealed

            for (id g in self.view.gestureRecognizers) {
                [self.view removeGestureRecognizer:g];
            }
            [self setRevealHiddenViewGestureRecognizerToSecondStep];
            return;
        }
        else if( self.firstSwipeToRevealSecret
                && gr.direction == UISwipeGestureRecognizerDirectionDown
                && gr.numberOfTouches == 1 )
        {
            // ok, reveal, and go back to first step
            [self setRevealHiddenViewGestureRecognizerToFirstStep];
        }
        else
        {
            // wrong kind of touch, go back to first step
            [self setRevealHiddenViewGestureRecognizerToFirstStep];
            return;
        }
    }
    else
    {
        // wrong kind of swipe, return
        return;
    }
        
    
    int transition = hiddenView ? UIViewAnimationTransitionFlipFromRight : UIViewAnimationTransitionFlipFromLeft;
    
    // animations for switching views
    // begin animations
	[UIView beginAnimations:nil	context:nil];
    // set transition
	[UIView setAnimationTransition:transition
                           forView:self.view
                             cache:YES];
    if( hiddenView )
    {
        
        [hiddenView removeFromSuperview];
    }
    else
    {
        // load hidden view
        [[NSBundle mainBundle] loadNibNamed:@"PushAdmin" owner:self options:nil];
        // add gestures to view
        [self addGestureRecToView:self.hiddenView
                        direction:UISwipeGestureRecognizerDirectionLeft
          numberOfTouchesRequired:3
         ];
        // add view
        [self.view addSubview:self.hiddenView];
        // set property to nil
        self.hiddenView = nil;
        [self.passwordTextField becomeFirstResponder];
    }
    // commit annimations
	[UIView setAnimationDuration:1];
	[UIView commitAnimations];
}

- (void)addGestureRecToView:(UIView*)view
                  direction:(UISwipeGestureRecognizerDirection)direction
    numberOfTouchesRequired:(NSUInteger)numberOfTouchesRequired

{
    UISwipeGestureRecognizer *g = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)] autorelease];
    
    g.direction = direction;
    g.numberOfTouchesRequired = numberOfTouchesRequired;
    [view addGestureRecognizer:g];

}

- (void)setRevealHiddenViewGestureRecognizerToSecondStep
{
    self.firstSwipeToRevealSecret = YES;
    [self addGestureRecToView:self.view
                    direction:UISwipeGestureRecognizerDirectionDown
      numberOfTouchesRequired:1];
}

- (void)setRevealHiddenViewGestureRecognizerToFirstStep
{
    [self addGestureRecToView:self.view
                    direction:UISwipeGestureRecognizerDirectionRight
      numberOfTouchesRequired:3
     ];
    
    self.firstSwipeToRevealSecret = NO;

}

- (void)viewDidLoad
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // gestures

    [self setRevealHiddenViewGestureRecognizerToFirstStep];
    
    // color
    [self setupColor];
    
    // set bar letter's color to white
    if( [self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)] )
    [self setNeedsStatusBarAppearanceUpdate];
    
    // setup player
    [self setupPlayer];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [super viewDidAppear:animated];

    // Turn on remote control event delivery
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    // Set itself as the first responder
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    // Turn off remote control event delivery
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    
    // Resign as first responder
    [self resignFirstResponder];
    
    [super viewWillDisappear:animated];
}


- (BOOL)canBecomeFirstResponder
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    return YES;
}

- (void)previousTrack:(id)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)nextTrack:(id)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    
    if (event.type == UIEventTypeRemoteControl) {
        
        switch (event.subtype) {
            case UIEventSubtypeRemoteControlPlay:
            case UIEventSubtypeRemoteControlPause:
                [self playOrPause: nil];
                break;
                
            case UIEventSubtypeRemoteControlPreviousTrack:
                [self previousTrack: nil];
                break;
                
            case UIEventSubtypeRemoteControlNextTrack:
                [self nextTrack: nil];
                break;
                
            default:
                break;
        }
    }
}


- (id)playButtonTitle
{
    return self.player2.playing ? @"Pause" : @"Play";
}

- (void)pause
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self.player2 pause];
    [self setPlayButtonTitle];
}

- (void)play
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self setUiToLoading];
    [self.player2 play];
    [self setPlayButtonTitle];
}

- (void)startPlayer
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self setUiToLoading];
    [self.player2 start];
}

- (IBAction)playOrPause:(id)sender {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    if( self.player2.playing )
    {
        [self pause];
    }
    else if( self.player2.isReadyToPlay )
    {
        [self play];
    }
    else
    {
        [self startPlayer];
    }

}

- (void)setUiToNotLoading
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self setPlayButtonTitle];
    [self.playButton setEnabled:YES];
    [self.ai stopAnimating];
    
}

- (void)setUiToLoading
{
    [self.playButton setEnabled:NO];
    [self.ai startAnimating];
}

- (IBAction)openJGDRadiocom:(id)sender {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.jgdradio.com.ar"]];
}

- (void)dealloc {
    [_playButton release];
    [_ai release];
    [_musicNameLabel release];
    [_passwordTextField release];
    [_hiddenView release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setPlayButton:nil];
    [self setAi:nil];
    [self setMusicNameLabel:nil];
    [self setPasswordTextField:nil];
    [self setHiddenView:nil];
    [super viewDidUnload];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    id newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if( [newString isEqualToString:The24242348234 ] )
    {
        [self didLogin];
    }
    
    return YES;
}

static BOOL shouldShowNormalViewAgain = NO;

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if( shouldShowNormalViewAgain )
    {
        [self removeHiddenView];
        shouldShowNormalViewAgain = NO;
    }
}


- (void)didLogin
{
    WritePushNotsViewController *c = [WritePushNotsViewController controller];
    c.willDissappear = ^(id controller){
        shouldShowNormalViewAgain = YES;
    };
    UINavigationController *n = [[[UINavigationController alloc] initWithRootViewController:c] autorelease];
    [self presentModalViewController:n animated:YES];

}
- (IBAction)loginHidden:(id)sender {
    
    if( ![self.passwordTextField.text isEqualToString:The24242348234] )
    {
        [[[[UIAlertView alloc] initWithTitle:@"Error" message:@"La contraseña no es correcta" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles: nil] autorelease] show];
        
        return;
    }
    
    [self didLogin];
    
}
@end
