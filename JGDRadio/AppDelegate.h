//
//  AppDelegate.h
//  JGDRadio
//
//  Created by Martín Mineo on 31/01/14.
//  Copyright (c) 2014 Martín Mineo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate, AVAudioPlayerDelegate>

@property (retain, nonatomic) IBOutlet UIWindow *window;

@end
