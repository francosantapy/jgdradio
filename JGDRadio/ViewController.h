//
//  ViewController.h
//  JGDRadio
//
//  Created by Martín Mineo on 31/01/14.
//  Copyright (c) 2014 Martín Mineo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@class Player;


@interface ViewController : UIViewController<UITextFieldDelegate> {

}
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *ai;
@property (retain, nonatomic) IBOutlet UIWebView *webPage;
@property (retain, nonatomic) Player *player2;
@property (retain, nonatomic) IBOutlet UILabel *musicNameLabel;

@property (retain, nonatomic) IBOutlet UIButton *playButton;

- (IBAction)playOrPause:(id)sender;

@end
