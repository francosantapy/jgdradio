//
//  Player.m
//  JGDRadio
//
//  Created by Franco Santa Cruz on 3/9/14.
//  Copyright (c) 2014 Martín Mineo. All rights reserved.
//

#import "Player.h"


@implementation Player

- (id)initWithURL:(NSString*)url
{
    self = [super init];
    if (self) {
        self.url = url;
    }
    return self;
}

- (void)initializePlayer
{
    self.player = [[[AVPlayer alloc] initWithURL:[NSURL URLWithString:self.url]] autorelease];
    [self.player addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    self.playing = NO;
}

- (BOOL)isReadyToPlay
{
    return self.player.status == AVPlayerItemStatusReadyToPlay;
}


- (void)play
{
    if( self.player.status != AVPlayerItemStatusReadyToPlay )
    {
        @throw @"Bad Status != AVPlayerItemStatusReadyToPlay";
    }
    self.playing = YES;
    [self makeStartedPlayingCall];
    [self.player play];
}

- (void)pause
{
    self.playing = NO;
    [self.player pause];
}

- (void)start
{
    [self initializePlayer];
}

- (void)retrieveMetadata
{
    // key for observing
    id key = @"timedMetadata";
    
    // if block and we're not observing the item
    if( self.metadataReady && self.observingMetadataItem != self.player.currentItem )
    {
        
        // stop observing previous item
        if( self.observingMetadataItem )
        {
            [self.observingMetadataItem removeObserver:self forKeyPath:key];
        }
        // add observer
        [self.player.currentItem addObserver:self forKeyPath:key options:NSKeyValueObservingOptionNew context:nil];
        
        // set current item
        self.observingMetadataItem = self.player.currentItem;
    }
    
    // if block set to nil, remove observer
    if( !self.metadataReady && self.observingMetadataItem )
    {
        [self.observingMetadataItem removeObserver:self forKeyPath:key];
    }
}

- (void)makeStartedPlayingCall
{
    CMTime time = self.player.currentItem.currentTime;
    if( time.value == 0 )
    {
        time = CMTimeMake(1, 10);
    }
    else
    {
        time.value += 1;
    }
    
    id times = @[
//                 [NSValue valueWithCMTime:CMTimeMake(1, 10)]
                 [NSValue valueWithCMTime:time]
                 ];
    [self.player addBoundaryTimeObserverForTimes:times queue:nil usingBlock:^(){
        if( self.startedPlaying )
        {
            self.startedPlaying();
        }
    }];

}

- (void) observeValueForKeyPath:(NSString *)keyPath
                       ofObject:(id)object
                         change:(NSDictionary *)change
                        context:(void *)context
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    if( [keyPath isEqualToString:@"status"] )
    {
        // status change
        // Log
        NSLog(@"New Status = %@", [self statusNameWithStatus:self.player.status]);
        
        // Actions
        switch( self.player.status)
        {
            case AVPlayerItemStatusReadyToPlay:
                if( self.readyToPlay )
                {
                    self.readyToPlay();
                }
                [self retrieveMetadata];
                break;
            case AVPlayerItemStatusFailed:
                [self initializePlayer];
                if( self.errorWhileTryingToPlay )
                {
                    self.errorWhileTryingToPlay(self.player.error);
                }
                break;
        }
        
    }
    else if( [keyPath isEqualToString:@"timedMetadata"] )
    {
        if( self.metadataReady )
        {
            self.metadataReady(self.player.currentItem.timedMetadata);
        }
    }
}


- (id)statusNameWithStatus:(AVPlayerItemStatus)status
{
    id dict = @{
                @(AVPlayerItemStatusUnknown):@"AVPlayerItemStatusUnknown",
                @(AVPlayerItemStatusReadyToPlay):@"AVPlayerItemStatusReadyToPlay",
                @(AVPlayerItemStatusFailed):@"AVPlayerItemStatusFailed"
    };
    return dict[@(status)];
}

- (void)dealloc
{
    [_url release];
    [_player release];
    if( self.readyToPlay ) Block_release(_readyToPlay);
    [_observingMetadataItem release];
    [super dealloc];
}


@end
