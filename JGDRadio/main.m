//
//  main.m
//  JGDRadio
//
//  Created by Martín Mineo on 31/01/14.
//  Copyright (c) 2014 Martín Mineo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {

        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
