//
//  WritePushNotsViewController.h
//  JGDRadio
//
//  Created by Franco Santa Cruz on 3/12/14.
//  Copyright (c) 2014 Martín Mineo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WritePushNotsViewController : UIViewController

+ (id)controller;

@property (nonatomic, copy) void (^willDissappear)(id controller);

@end
