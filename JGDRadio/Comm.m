//
//  Comm.m
//  JGD Radio
//
//  Created by Franco Santa Cruz on 3/17/14.
//  Copyright (c) 2014 Martín Mineo. All rights reserved.
//

#import "Comm.h"
#import "AppDelegate.h"
#import <DevaFwk/DevaFwk.h>
#include <sys/types.h>
#include <sys/sysctl.h>

static id _url = @"http://perlas2.elartedevivirpy.org:8080/JGD-Push-Server";
//static id _url = @"http://192.168.11.6:8080/JGD-Push-Server";

static id secret = @"DAhWA9pOzYVTUId5gl6poy4QmEwNMj4wwGghwvIttyCr5LfS1Q6wzybHVsyDiGzc";

@implementation Comm

+ (id)createToken:(id)timeMillis
{
    id token = [[[[[[@"venga rafa" stringByAppendingString:secret] stringByAppendingString:@" "] stringByAppendingString:@"xfG"] stringByAppendingString:timeMillis] stringByAppendingString:@" "] sha1];
    return token;
}

+ (NSString*) currentTimeMillis{
    return [NSString stringWithFormat:@"%.0lf",[[NSDate date] timeIntervalSinceDate:[NSDate dateWithTimeIntervalSince1970:0]]*1000];
}


+ (CommError)addDevice:(NSString*)pushNotDeviceToken
{
    id stamp = self.currentTimeMillis;
    id ret = [NSURLConnection post:[_url stringByAppendingString:@"/device/add"]
                            params:@{
                                     @"deviceToken":pushNotDeviceToken,
                                     @"token": [self createToken:stamp],
                                     @"appVersion":self.appVersion,
                                     @"clientModel":self.platformCode,
                                     @"clientType":@"IOS",
                                     @"stamp":stamp,
                                     @"clientOsVersion":[[UIDevice currentDevice] systemVersion],
                                     @"deviceUDID":[[[UIDevice currentDevice] identifierForVendor] UUIDString],
                                     @"deviceLang":[[NSLocale preferredLanguages] objectAtIndex:0],
                                     @"deviceRegion":[[NSLocale currentLocale] objectForKey: NSLocaleCountryCode],
                                     
                                     }
              ];
    id data = ret[@"data"];
    id error = ret[@"error"];
    id response = ret[@"response"];
    
    if( data )
    {
        id string = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
        NSLog(@"ret string = %@", string);
        id json = [string objectFromJSONString];
        id error = json[@"error"];
        NSLog(@"error = %@", error);
        NSLog(@"response = %@", json[@"response"]);
        if( error )
        {
            return CommErrorProtocolError;
        }
    }
    if( error )
    {
        NSLog(@"http error = %@", error);
        return CommErrorHttpError;
    }
    if( response )
    {
        NSLog(@"http response = %@", response);
    }
    return CommErrorOk;
}

+ (CommError)pingDevice
{
    id stamp = self.currentTimeMillis;
    id ret = [NSURLConnection post:[_url stringByAppendingString:@"/device/ping"]
                            params:@{
                                     @"token": [self createToken:stamp],
                                     @"appVersion":self.appVersion,
                                     @"clientModel":self.platformCode,
                                     @"clientType":@"IOS",
                                     @"stamp":stamp,
                                     @"clientOsVersion":[[UIDevice currentDevice] systemVersion],
                                     @"deviceUDID":[[[UIDevice currentDevice] identifierForVendor] UUIDString],
                                     @"deviceLang":[[NSLocale preferredLanguages] objectAtIndex:0],
                                     @"deviceRegion":[[NSLocale currentLocale] objectForKey: NSLocaleCountryCode],
                                     
                                     }
              ];
    id data = ret[@"data"];
    id error = ret[@"error"];
    id response = ret[@"response"];
    
    if( data )
    {
        id string = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
        NSLog(@"ret string = %@", string);
        id json = [string objectFromJSONString];
        id error = json[@"error"];
        NSLog(@"error = %@", error);
        NSLog(@"response = %@", json[@"response"]);
        if( error )
        {
            return CommErrorProtocolError;
        }
    }
    if( error )
    {
        NSLog(@"http error = %@", error);
        return CommErrorHttpError;
    }
    if( response )
    {
        NSLog(@"http response = %@", response);
    }
    return CommErrorOk;
}

+ (CommError)sendPushNot:(id)text
{
    id stamp = self.currentTimeMillis;
    id ret = [NSURLConnection post:[_url stringByAppendingString:@"/push/send"]
                            params:@{
                                     @"token":[self createToken:stamp],
                                     @"stamp":stamp,
                                     @"text":text,
                                     }
              ];
    id data = ret[@"data"];
    id error = ret[@"error"];
    id response = ret[@"response"];
    

    if( data )
    {
        id string = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
        NSLog(@"ret string = %@", string);
        id json = [string objectFromJSONString];
        if( !json )
        {
            // if no json, no parseable data, it's an HTTP error probably
            return CommErrorHttpError;
        }
        id error = json[@"error"];
        NSLog(@"error = %@", error);
        NSLog(@"response = %@", json[@"response"]);
        if( error )
        {
            return CommErrorProtocolError;
        }
    }
    else
    {
        // if no data, it's an HTTP error probably
        return CommErrorHttpError;
        
    }
    if( error )
    {
        NSLog(@"http error = %@", error);
        return CommErrorHttpError;
    }
    if( response )
    {
        NSLog(@"http response = %@", response);
    }
    return CommErrorOk;
}

+ (NSString *) platformCode {
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:[NSString defaultCStringEncoding]];
    free(machine);
    return platform;
}


+ (id)appVersion
{
    NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    return appVersion;
}


@end
