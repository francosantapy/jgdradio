//
//  AppDelegate.m
//  JGDRadio
//
//  Created by Martín Mineo on 31/01/14.
//  Copyright (c) 2014 Martín Mineo. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <DevaFwk/DevaFwk.h>
#import "Comm.h"
@implementation AppDelegate

- (id)init
{
    self = [super init];
    if (self) {
        [[AVAudioSession sharedInstance] setDelegate: self];
        [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error:nil];
     //   [[AVAudioSession sharedInstance] setActive:YES];
        
    }
    return self;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSError *error = nil;
    [[AVAudioSession sharedInstance] setActive:YES error:&error];
    NSLog(@"%@", error);
    
//    [self.window set]
    
    ViewController *vc = [[[ViewController alloc] init] autorelease];
    [self.window setRootViewController:vc];
    [self.window makeKeyAndVisible];
    
    // Override point for customization after application launch.
    
     [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
//    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    
    [self executeCodeInBackground:^(){
        
        
        CommError error = [Comm pingDevice];
        NSLog(@"error = %d", error);
        
        
    }];

    
    return YES;
}

- (NSString*)convertTokenToString:(NSData*)deviceToken
{
    NSMutableString *token = [NSMutableString string];
    const unsigned char *data = [deviceToken bytes];
    for( int i = 0; i < deviceToken.length; i++ )
    {
        [token appendFormat:@"%02x", data[i]];
    }

    return token;
}



- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *token = [self convertTokenToString:deviceToken];
    
    [self executeCodeInBackground:^(){


            [Comm addDevice:token];


    }];
}



- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    NSLog(@"Error in registration. Error: %@", err);
}



							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)dealloc
{
    [self.window release];
    
    [super dealloc];
}

@end
