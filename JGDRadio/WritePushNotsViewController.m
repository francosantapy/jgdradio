//
//  WritePushNotsViewController.m
//  JGDRadio
//
//  Created by Franco Santa Cruz on 3/12/14.
//  Copyright (c) 2014 Martín Mineo. All rights reserved.
//

#import "WritePushNotsViewController.h"
#import <DevaFwk/DevaFwk.h>
#import "Comm.h"

@interface WritePushNotsViewController ()
@property (retain, nonatomic) IBOutlet UITextView *textArea;

@end

@implementation WritePushNotsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.textArea becomeFirstResponder];
}

+ (id)controller
{
    return [[[WritePushNotsViewController alloc] init] autorelease];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Enviar" style:UIBarButtonItemStyleBordered target:self action:@selector(send:)] autorelease];
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Cerrar" style:UIBarButtonItemStylePlain target:self action:@selector(cancel:)] autorelease];
    
    self.title = @"Enviar Push";
}

- (void)cancel:(id)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    if( self.willDissappear )
    {
        self.willDissappear(self);
    }
    [self dismissModalViewControllerAnimated:YES];
    self.willDissappear = nil;
}

- (void)send:(id)sender //completion:(void (^)(CommError errCode))completion
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    id text = self.textArea.text;
    
    if( [text length] == 0 )
    {
        [[[[UIAlertView alloc] initWithTitle:@"Error" message:@"Escriba un mensaje para poder enviarlo" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles: nil] autorelease] show];
        return;
    }
    id msg = [NSString stringWithFormat:@"Estás seguro que querés enviar \"%@\" a todos los usuarios de JGD Radio?", text];
    [UIAlertView showMessage:msg title:@"Seguro?" buttons:@[@"Enviar"] cancelButtonTitle:@"Cancelar" block:^(BOOL cancel, NSInteger ix) {
       if( !cancel )
       {
           // enviar
           NSLog(@"enviar \"%@\" a todos", text);

           id hud = [MBProgressHUD hudWithTitle:@"Procesando" view:self.view];
           [hud showAnimated:YES whileExecutingBlock:^(){

               CommError ret = [Comm sendPushNot:text];
               if( ret != CommErrorOk )
               {
                   id error = [NSString stringWithFormat:@"Ocurrió un error al enviar el mensaje. Ponete en contacto con Franco para ver qué pasa o volvé a intentar en unos segundos. Decile q fue un (%@)", ret == CommErrorHttpError ? @"HttpError" : @"ProtocolError"];
                   [self executeCodeOnMainThread:^{
                       [UIAlertView error:error];
                   }];
               }
               else
               {
                   [self executeCodeOnMainThread:^{
                       [UIAlertView showMessage:@"Los mensajes fueron enviados con éxito" title:@"Mensaje" buttons:nil cancelButtonTitle:@"Ok" block:^(BOOL cancel, NSInteger ix) {
                           self.textArea.text = nil;
                       }];
                   }];
               }
           }];
       }
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_textArea release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTextArea:nil];
    [super viewDidUnload];
}
@end
